<?php
namespace App\Contracts;

Interface ImportProductsContract
{
    public function getRemoteProductsCount($url);
    public function checkForProducts();
    public function checkIfBrandExist();
    public function checkIfCategoryExist();
    public function updateExistingProduct();
    public function createProducts();
    public function checkForProductOnPage();
}
    // public function FunctionName();}
