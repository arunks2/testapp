<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';
    protected $fillable = ['category_name','category_img'];
    protected $dates = ['created_at','deleted_at','updated_at'];

    public function products()
    {
    	return $this->hasMany('App\Product','category_id');
    }
}
