<?php

namespace App;
use  App\Exceptions\ModalNotFoundException;
use Auth;
use Illuminate\Database\Eloquent\Model;
use DB;
class Product extends Model
{
	protected $table = 'products';
    protected $fillable =['sku','name','img','attributes','more-info','brand_id','category_id','is_warranty_available','is_finance_available',
    'price'];
    protected $dates = ['created_at','updated_at','deleted_at'];

    public function brand()
    {
    	return $this->belongsTo('App\Brand','brand_id');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category','category_id');
    }
    public function users()
    {
        return $this->belongsToMany('App\User','user_products','product_id','user_id');
    }

    public function usersLiked()
    {
        return $this->belongsToMany('App\User','user_product_like','product_id','user_id')->withPivot('likeability');
    }

    public static function getProducts($request)
    {
    	$offset = isset($request['offset']) ? $request['offset'] : 0 ;
    	$chunk = isset($request['chunk']) ? $request['chunk'] : 999999;
    	$category_id = isset($request['category_id']) ? $request['category_id'] : -1;
        $type = isset($request['type']) ? $request['type'] : -1;
    	$search = isset($request['search']) ? $request['search'] : '';
    	if($search == NULL) {
    		$search = '';
    	}
    	if($category_id != -1):
    		$category = ProductCategory::find($category_id);
    		$results = $category->load(['products' => function ($query) use ($chunk, $offset,$search) {
    			$query->where('Name','like','%'.$search.'%')->skip($offset)->limit($chunk)->orderBy('id', 'desc')->get();
    		}]);
    		return $results->products;
    	elseif ($type != -1):
    		$brand = Brand::find($type);
    		$results = $stone->load(['products' => function ($query) use ($chunk, $offset, $search) {
    			$query->where('Name','like','%'.$search.'%')->skip($offset)->limit($chunk)->orderBy('id','desc')->get();
    		}]);
    		return $results->products;
    	else:
    			$results = Product::where('name','like','%'.$search.'%')->orWhere('sku','like','%'.$search.'%')->skip($offset)->limit($chunk)->orderBy('id', 'desc')->get();
    			return $results;
    	endif;
    }

    public function createProduct($request)
    {
    	if(isset($request['brand_id'])) {
            try {
                $brand = Brand::findorFail($request['brand_id']);
            } catch (\Exception $e) {
                throw new ModalNotFoundException;
            }
        }
    	if(isset($request['category_id'])) {
            try {
                $category = Category::findorFail($request['category_id']);
            } catch (\Exception $e) {
                throw new ModalNotFoundException;
            }
        }
        $product = $request->all();
        $product = Product::create($product);
         return $product;
    }
    public static function getProductByIds($product_array)
    {
        $products = Collect();
        foreach($product_array as $id)
        {
            $product = Product::find($id);
            Product::getProperties($product);
            $products->push($product);
        }
        return $products;    
    }

    public static function getProperties($product,$by=Null) {
        $product->likes = 0;
        $product->dislikes = 0;
        // $user = Auth::
        $user = Auth::User();
        $friends = $user->friends;
        if($by) {
            $friends = $by;
        }
        
        foreach($friends as $f) {
            if($f->pivot->accepted == 1) {
                $likeability = DB::table('user_product_like') //optimizing performance
                                    ->where('user_id',$f->id)
                                    ->where('product_id',$product->id)
                                    ->first();
                if($likeability) {
                    if($likeability->likeability == -1) {
                        $product->dislikes++;
                    }
                    else {
                        if($likeability->likeability == 1) {
                            $product->likes++;
                        }
                    }
                }
            }
        }
        return $product->load(['brand','category']);
    }

    public static function getFriendsProduct(User $friend)
    {   
       
        $products = $friend->products;
        $friends = $friend->friends;
        foreach($products as $product) {
          Product::getProperties($product,$friends);
        }
        return $products;   
    }

    public static function checkIfFriendHasProduct(User $friend, Product $product)
    {
        $result = $friend->products()->where('product_id',$product->id)->first();
        if($result) {
            return true;
        }
        return false;
    }
    
}
