<?php
namespace App\Repositories;
use App\Contracts\SMScontract;
use  App\Exceptions\ModalNotFoundException;
use Hash;
use Auth;
use JWTAuth;
use App\User;
use Mail;
use DB;
use App\Product;
use App\Mail\ChangePassword;
use App\Mail\DeclineRequest;
use App\Mail\SendFriendRequest;
use App\Mail\SendWishlist;

class UserRepositories {
	private $sms;
	public function __contruct(SMScontract $sms)
	{
		$this->sms = $sms;
	}
	public static function generateToken(User $user)
    {
        $token = JWTAuth::fromUser($user);
        JWTAuth::setToken($token);
        $token = 'Bearer ' . $token;
        $data['token'] = $token;
        $data['user'] = $user;
        return $data;
    }
	public static function getUsersByFilters($search, $data)
    {
        $currentUser = Auth::User();
        $search = json_decode($search);
        $users = NULL;
        $users = DB::table('users');
        foreach ($search as $key => $value) {
            $value = User::makeSearchAble($value);
            if($key != 'status') {
                $users = $users->where($key,'like',$value);
            }
        }
        $users = $users->where('id','!=',$currentUser->id);
        $sort_order = isset($data['sort_order']) ? $data['sort_order'] : 'desc';
        $sort_by = isset($data['sort_by']) ? $data['sort_by'] : 'id';
        $offset = isset($data['offset']) ? $data['offset'] : 0 ;
        $chunk = isset($data['chunk']) ? $data['chunk'] : 999999;
        $users = $users->skip($offset)->limit($chunk)->OrderBy($sort_by,$sort_order)->get();
        $myFriends = $currentUser->myFriends; // my invited friends
        $friendsWith = $currentUser->friendsWith; // who invited me
        foreach ($users as $user) {

			$myFriendFilter = $myFriends->where('pivot.friend_id',$user->id)->where('pivot.user_id',$currentUser->id);
			$friendsWithFilter = $friendsWith->where('pivot.friend_id',$currentUser->id)->where('pivot.user_id', $user->id);
			if($myFriendFilter->count() != 0) {
			  $user->status = 1; //To be Friend has not accepted the friendship yet
			  $filtered = $myFriends->where('pivot.friend_id',$user->id)->where('pivot.user_id',$currentUser->id)->where('pivot.accepted',true);
			  if($filtered->count() !=0) {
			      $user->status = 2; // person has accepted my friendship, we both are friend
			  }
			}
			elseif($friendsWithFilter->count() != 0 ) {
			  $user->status = 3; // I have received the friend request, but not accepted it
			  $filtered = $friendsWith->where('pivot.friend_id',$currentUser->id)->where('pivot.user_id', $user->id)->where('pivot.accepted',true);
			  if($filtered->count() !=0) {
			      $user->status = 2; // I accepted the friendship, we both are friend
			  }
			}
			else {
			  $user->status = 4; // open for invitation, and be invited over
			}
        }
        return $users;
    }

    public static function getUsers($data)
    {   
        $users = NULL;
        $users = DB::table('users');
        $sort_order = isset($data['sort_order']) ? $data['sort_order'] : 'desc';
        $sort_by = isset($data['sort_by']) ? $data['sort_by'] : 'id';
        $offset = isset($data['offset']) ? $data['offset'] : 0 ;
        $chunk = isset($data['chunk']) ? $data['chunk'] : 999999;
        $users = $users->skip($offset)->limit($chunk)->OrderBy($sort_by,$sort_order)->get();
        $currentUser = Auth::User();
        $users = $users->where('id','!=',$currentUser->id);
        $myFriends = $currentUser->myFriends; // my invited friends
        $friendsWith = $currentUser->friendsWith; // who invited me
        // return $myFriends;
        foreach ($users as $user) {

            $myFriendFilter = $myFriends->where('pivot.friend_id',$user->id)->where('pivot.user_id',$currentUser->id);
            $friendsWithFilter = $friendsWith->where('pivot.friend_id',$currentUser->id)->where('pivot.user_id', $user->id);
            if($myFriendFilter->count() != 0) {
                $user->status = 1; //have not accepted the friendship yet
                $filtered = $myFriends->where('pivot.friend_id',$user->id)->where('pivot.user_id',$currentUser->id)->where('pivot.accepted',true);
                if($filtered->count() !=0) {
                    $user->status = 2; // person has accepted my friendship, we both are friend
                }
            }
            elseif($friendsWithFilter->count() != 0 ) {
                $user->status = 3; // I have received the friend request, but not accepted it
                $filtered = $friendsWith->where('pivot.friend_id',$currentUser->id)->where('pivot.user_id', $user->id)->where('pivot.accepted',true);
                dd($filtered->count());
                if($filtered->count() !=0) {
                    $user->status = 2; // I accepted the friendship, we both are friend
                }
            }
            else {
                $user->status = 4; // open for invitation, and be invited over
            }
        }
         return $users;
    }
   
    public static function makeSearchAble($value)
    {
        if($value == NULL) {
            $value = '%'.''.'%';
        }
        $length = strlen($value);
        if(strpos($value, '%') !==0 && strpos($value, '%') !== $length-1) {
            $value = '%'.$value.'%';
        }
        return $value;
    }
	public static function sendRequest($friendId)
    {
        // $user = Auth::User(); 
        // DB operation are faster this way
        try {
            $friend = User::findorFail($friendId);
        }   catch (\Exception $e) {
                throw new ModalNotFoundException;
        }
        $user = Auth::User();
        $result = DB::table('user_friends')->insert(
            [
            'user_id' => $user->id,
            'friend_id' => $friend->id,
            'accepted'  => false
            ] 
            );
        if($result) {
            Mail::to($user->email)->send(new SendFriendRequest($user, $friend));
            if($friend->is_mobile_verified) {
                $message = "$user->name ". "has sent you a friend request on MyApp";
                $this->sms->sendOTP($message, $user->mobile_number);
            }
            return true;
        }
    }


    public static function sendOTPForPasswordChange($sms, $email)
    {
        $user = User::where('email','=',$email)->first();
        if($user) {
            $otp = str_random(6);
            $user->otp = $otp;
            $user->save();
            if($user->is_mobile_verified) {
                $message = "Your OTP for password change is ". $otp;
                $sms->sendOTP($message, $user->mobile_number);
                return true;
            }
            if($user->is_email_verified) {
                Mail::to('noreply@myapp.com')->send(new ChangePassword($user));
                return true;
            }
            return false;
        }
        return false;
        
    }

    // TODO::wanted to separate it to some class, but not enough time
    public static function authenticateUser($email, $password)
    {
    $user = User::where('email', '=', $email)->first();
            if($user) {
                if (Hash::check($password, $user->password)) {
                    return $user;
                } else {
                    return false;
                }
            }
            else {
                return false;
            }
    }

    public static function checkForValidOTP(User $user, $otp, $type)
    {
       if($user->$type == $otp) {
            return $user;
       }
       else {
            return false;
       }
    }

    public static function deleteRelationShip($request)
    {
    	try {
    		$friend = User::findorFail($request['friend_id']);
    	} catch (\Exception $e) {
                throw new ModalNotFoundException;
        }
        $currentUser = Auth::User();
        $type = $request['type'];
        if($type == 'delete') { // current user wants to delete or the invitee wants to decline
           $relationship = DB::table('user_friends')
                                ->where('user_id','=',$currentUser->id)
                                ->where('friend_id','=',$friend->id)
                                ->first();
            if($relationship) {
                $result = DB::table('user_friends')->delete($relationship->id);
                return true;
            }
            else {
                return false;
            }
        }
        else if($type == 'denied') { // invitee declines the request
        	$relationship = DB::table('user_friends')
        	                     ->where('user_id','=',$friend->id)
        	                     ->where('friend_id','=',$currentUser->id)
        	                     ->first();
        	 if($relationship) {
        	    $result = DB::table('user_friends')->delete($relationship->id);
                Mail::to($currentUser)->send(new DeclineRequest($currentUser, $friend,$request['type']));
        	     return true;
        	 }
        	 else {
        	     return false;
        	 }
        }
        else if ($request['type'] == 'unfriend') // invitee declines the request
        {
            $relationship = DB::table('user_friends') //inviter wants to delete
                                 ->where('user_id','=',$currentUser->id) 
                                 ->where('friend_id','=',$friend->id)
                                 ->where('accepted',true)
                                 ->first();
            $relationshipTwo =  DB::table('user_friends') //invitee wants to delete
                                 ->where('user_id','=',$friend->id)
                                 ->where('friend_id','=',$currentUser->id)
                                 ->where('accepted',true)
	                             ->first();
             if($relationship) {
                 $result = DB::table('user_friends')->delete($relationship->id);
                Mail::to($currentUser)->send(new DeclineRequest($currentUser, $friend,$request['type']));

                 return true;
             }
             else if ($relationshipTwo) {
                $result = DB::table('user_friends')->delete($relationshipTwo->id);
                Mail::to($currentUser)->send(new DeclineRequest($currentUser, $friend,$request['type']));
                return true;                
             }
             else {
                return false;
            }
        }
        return false;
    }

    public static function checkIfRelationShipExist(User $user, User $friend)
    {
       $friends = $user->friends;
        $filteredFriends = $friends->where('pivot.accepted',true)->where('id',$friend->id);
        if($filteredFriends->count() !=0) {
           return true;
        }
        return false;
    }

    public static function checkIfUserHasLikedProduct(User $user, Product $product, $type)
    {
       $likedRelationShip = DB::table('user_product_like')
                                    ->where('user_id', $user->id)
                                    ->where('product_id', $product->id)
                                    ->first();
       if($likedRelationShip) {
           $likedRelationShip = DB::table('user_product_like')
                                    ->where('user_id', $user->id)
                                    ->where('product_id', $product->id)
                                    ->update(['likeability' => $type]);
       }
       else {
           $likedRelationShip = DB::table('user_product_like')
                                    ->insert([
                                        'user_id' => $user->id,
                                        'product_id' => $product->id,
                                        'likeability' => $type
                                    ]);
       }
       return $likedRelationShip;
    }

    public static function sendWishList($users_array, $products)
    {
        foreach($users_array as $id) {
            try {
                $user = User::findorFail($id);
            } catch (\Exception $e) {
                throw new ModalNotFoundException;
            }
         Mail::to($user->email)->send(new SendWishlist($user,$products));
        }
        return true;
    }
}