<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Contracts\SMScontract;
use App\Repositories\UserRepositories;
use JWTAuth;
use Validator;
use Mail;
use Auth;
use Goutte\Client;
use File;
use App\Brand;
use App\Category;
use App\Product;
// use \Goutte\Client;
use App\Mail\ChangePassword;
use App\Events\UserSignedUp;

class AuthenticationController extends Controller {
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	protected $client;
	public function __contruct(\Goutte\Client $client) {
		$this->client = $client;
	}
	public function store(Request $request) {
		$validation = Validator::make($request->all(), [
			'name' 						=> 'required',
			'password' 					=> 'required',
			'email'        				=> 'required|unique:users,email',
			'mobile_number'            => 'required|unique:users|mobile_validation'
		]);

		if ($validation->fails()) {
			$errors = $validation->errors();
			return response()->json($errors, 400);
		}
		$user =  Auth::User();
		$request['password'] = Hash::make($request->password);
		$request['sms_otp'] = str_random(6);
		$request['email_otp'] = str_random(6);
		$user = $request->only(['name','password','email','mobile_number','sms_otp','email_otp']);
		$user = User::create($user);
		Event(new UserSignedUp($user));
		$data = UserRepositories::generateToken($user);	
		return response()->json($data, 201);
	}

	public function signIn(Request $request) {

		$validation = Validator::make($request->all(), [
			'email' => 'required',
			'password' => 'required',
		]);
		if ($validation->fails()) {
			$errors = $validation->errors();
			return response()->json($errors, 400);
		}
		$user = UserRepositories::authenticateUser($request['email'], $request['password']);
		if ($user) {
			$data = UserRepositories::generateToken($user);
			return response()->json($data, 200);
		} else {
			return response()->json(['error' => 'Unauthorized'], 401);
		}
	}

	public function logout(Request $request) {
		JWTAuth::invalidate(JWTAuth::getToken());
		return response()->json(['true'], 200);
	}

	public function authenticatedUser()
	{
	    try {
	        if (!$user = JWTAuth::parseToken()->authenticate()) {
	            return response()->json(['user_not_found'], 404);
	        }
	    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
	        return response()->json(['token_expired'], $e->getStatusCode());
	    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
	        return response()->json(['token_invalid'], $e->getStatusCode());
	    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
	        return response()->json(['token_absent'], $e->getStatusCode());
	    }
	    return response()->json($user,201);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		$validation = Validator::make($request->all(), [
			'name' => 'required'
 		]);
		if ($validation->fails()) {
			$errors = $validation->errors();
			return response()->json($errors, 400);
		}
		$user = User::findorFail($id);
		if(isset($request['password'])):
			$request['password'] = Hash::make($request->password);
		endif;
		$result = $user->update($request->all());
		return response()->json($user, 201);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$user = User::findorFail($id);
		$result = User::destroy($user->id);
		return response()->json(true, 201);
		
	}

	public function resetPassword(Request $request)
	{
		$validation = Validator::make($request->all(),[
			'email' => 'required'
		]);
		$data['user'] = User::where('email','=',$request['email'])->first();
		Mail::send('emails.passwordchange', $data, function($message) use ($setting) {
	    $message->to($setting->email);
	    $message->subject('Password Request Change');
		}); 
		return response()->json('success',201);
	} 

	public function verifySmsOTP(Request $request)
	{
		$validation = Validator::make($request->all(),[
			'otp'   => 'required'
		]);
		if($validation->fails()) {
			$errors = $validation->errors();
			return response()->json(['Bad Request'], 400); //will override if their is time.
		}
		$user = Auth::User();
		$user = UserRepositories::checkForValidOTP($user,$request['otp'],'sms_otp');
		if($user) {
			$user->is_mobile_verified = 1;
			$user->sms_otp = Null;
			$user->save();
			return response()->json(['success'=> 'verified'], 201); // Unauthorized
		}
		return response()->json(['error' => 'Unauthorized'], 401);
	}

	public function verifyEmailOTP(Request $request)
	{
		$validation = Validator::make($request->all(),[
			'otp'   => 'required'
		]);
		if($validation->fails()) {
			$errors = $validation->errors();
			return response()->json(['Bad Request'], 400); //will override if their is time.
		}
		$user = Auth::User();
		$user = UserRepositories::checkForValidOTP($user, $request['otp'],'email_otp');
		if($user) {
			$user->is_email_verified = 1;
			$user->email_otp = Null;
			$user->save();
			return response()->json(['success'=> 'verified'], 201); 
		}
		return response()->json(['error' => 'Unauthorized'], 401); // Unauthorized
	}

	public function sendOTP(Request $request, SMScontract $sms)
	{
		$validation = Validator::make($request->all(),[
			'email' => 'required|email'
		]);
		if($validation->fails()) {
			$errors = $validation->errors();
			return response()->json($errors, 400);
		}
		$result = UserRepositories::sendOTPForPasswordChange($sms, $request['email']);
		if($result) {
			return response()->json(['success'], 200);
		}
		return response()->json(['error'=>'Contact your Admin'], 405);

	}

	public function verifyOTP(Request $request)
	{
		$validation = Validator::make($request->all(),[
			'email' => 'required|email',
			'otp' => 'required'
		]);
		if($validation->fails()) {
			$errors = $validation->errors();
			return response()->json($errors, 400);
		}
		$user = User::where('email','=',$request['email'])->first();
		$user = UserRepositories::checkForValidOTP($user, $request['otp'],'otp');
		if($user) {
			$data = UserRepositories::generateToken($user);
			return response()->json($data, 200);
		}
		return response()->json(['error' => 'Unauthorized'], 401);
	}
	
	public function changePassword(Request $request)
	{
		$validation = Validator::make($request->all(),[
			'password' => 'required|min:6'
		]);
		if($validation->fails()) {
			$errors = $validation->errors();
			return response()->json(['error'=>'Bad Request'], 404);
		}
		$user = Auth::user();
		$request['password'] = Hash::make($request['password']);
		$user->password = $request['password'];
		$user->save();
		$data = UserRepositories::generateToken($user);
		return response()->json($data, 200);
	}
	public function crawlPage()
	{		
		$client = new \Goutte\Client();
		$dom = $client->request('GET', 'https://www.appliancesdelivered.ie/search/small-appliances?sort=price_desc#');
		$results = [];
        foreach($dom->filter('div.search-results-product') as $element) {
        	$results[] = ($element->textContent);
        }
        $productImage = array();
        $brandImage = array();
        $productName = array();
        $brands = array();
        $attributes = array();
        $previousPrice = array();
        $newPrice = array();
        $availabilty = array();
        $links = array();
        foreach ($dom->filter('img.img-responsive,img.article-brand,div.col-xs-12>h4>a, ul.result-list-item-desc-list, h5.price-previous, h3.section-title, a.item-info-more') as $element) {
        	if ($element->getAttribute('class') =='img-responsive') { //product image
        		$productImage[] = $element->getAttribute('src');
        	}
        	if($element->getAttribute('class') == 'article-brand') { // brand img
        		$brandName = $element->getAttribute('alt');
        		$brandName_array = explode(' ',$brandName);
        		$brand = $brandName_array[0];
        		$brands[] = $brand;
        		$brandImage[$brand] = $element->getAttribute('src');
        	}
        	if($element->nodeName == 'a' && $element->getAttribute('class') != 'item-info-more' && $element->getAttribute('class') != 'section-title') { // node name
        		$productName[] = trim($element->textContent);
        		$links[] = trim($element->getAttribute('href'));
        	}
        	if ($element->nodeName == 'ul') {
        		$attributes[] = trim($element->textContent);
        	}
        	if ($element->getAttribute('class') == 'section-title') {
        		$newPrice[] = trim($element->textContent);
        	}
        	if ($element->getAttribute('class') == 'item-info-more') {
        		$availabilty[] = trim($element->textContent);
        	}
        }
        // dd(($results[0]));
  		// create brands 
  		$brands = array();
  		$skus = array();
  		foreach ($productName as $name) {
  			$name_array = explode(' ', $name);
  			$length = count($name_array);
  			$brands[] = $name_array[0];
  			$skus[] = $name_array[$length-1];
  		}
  		
  		// display products
  		$category = Category::where('category_name','Small Appliances')->first();
  		if(!$category) {
  			$category = Category::create(['category_name' => 'Small Appliances'])->first();
  		}
  		$length = count($productName);
  		for($i=0; $i < $length; $i++){
  			$product = array();
  			$product['name'] = $productName[$i];
  			$product['img'] = $productImage[$i];
  			$product['attributes'] = $attributes[$i];
  			$product['more-info'] = $links[$i];
  			$product['sku'] = $skus[$i];
  			// return response()->json(strpos('Extended Warranty Available',$results[0]), 200);

  			if(in_array('Warranty', explode(' ',$results[0])))
       		{
  				$product['is_warranty_available'] = 1;
  			}
  			if(in_array('Finance', explode(' ',$results[0]))) {
  				$product['is_finance_available'] = 1;
  			}
			$tempImage = array_key_exists($brands[$i], $brandImage) ? $brandImage[$brands[$i]] : Null;
  			$brand = Brand::where('brand_name','=',$brands[$i])->first();
  			if($brand) {
  				$brand->brand_img = $tempImage;
  				$brand->save();
  			}
  			else {
  				$brand = Brand::create(['brand_name' => $brands[$i],'brand_img'=>$tempImage]);
  			}
  			$product['brand_id'] = $brand->id;
  			$product['category_id'] = $category->id;
  			$newPrice[$i] = str_replace('€', '', $newPrice[$i]);
  			$newPrice[$i] = str_replace(',', '', $newPrice[$i]);
  			$product['price'] = (float)str_replace('€', '', $newPrice[$i]);
  			// dd($product);
  			$tempProduct = Product::where('sku', $product['sku'])->first();
  			if($tempProduct) {
  				$tempProduct->update($product);
  			}
  			else {
  				$product = Product::create($product);
  			}
  		}
  		return response()->json(true, 201);
	}
}
