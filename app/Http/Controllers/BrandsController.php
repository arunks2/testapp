<?php

namespace App\Http\Controllers;
use  App\Exceptions\ModalNotFoundException;
use Illuminate\Http\Request;
use Validator;
use App\Brand;
class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        return response()->json($brands, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'brand_name' => 'required',
            'brand_img' => 'sometimes|url' 
        ]);
        if($validation->fails()) {
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        $brand = $request->all();
        $brand = Brand::create($brand);
        return response()->json($brand, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
           $brand = Brand::findorFail($id);
        }catch (\Exception $e) {
           throw new ModalNotFoundException;
       }
       return response()->json($brand->load('products'), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'brand_name' => 'required',
            'brand_img'  => 'sometimes|url'
        ]);
        if($validation->fails()) {
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        try {
            $brand = Brand::findorFail($id);
        }catch (\Exception $e) {
            throw new ModalNotFoundException;
        }
        $brand->update($request->all());
        return response()->json(true, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $brand = Brand::findorFail($id);
        } catch (\Exception $e) {
          throw new ModalNotFoundException;
        }
        $result = $brand->delete();
        return response()->json($result, 301);
    }
}
