<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Exceptions\ModalNotFoundException;
use Validator;
use App\User;
use App\Repositories\UserRepositories;
use Auth;
use Mail;
use DB;
use App\Mail\RequestAccepted;

class FriendsController extends Controller
{
	private $user;

	public function __construct() {
		$this->middleware(function ($request, $next) {
            $this->user = Auth::User();
            return $next($request);
        });
	}
    public function index(Request $request)
    {
        if(isset($request['filters'])) {
            $users = UserRepositories::getUsersByFilters($request['filters'],$request);
            return response()->json($users, 201);
        }
        else {
            $users = UserRepositories::getUsers($request);
            return response()->json($users, 201);
        }
    }
    public function addFriend(Request $request)
    {
    	$validation = Validator::make($request->all(),[
    		'friend_id' => 'required|numeric'
    	]);
    	if($validation->fails()) {
    		$errors = $validation->errors();
    		return response()->json($errors, 400);
    	}
        $result = UserRepositories::sendRequest($request['friend_id']);
        if($result) {
            return response()->json(['success' => 'Request Sent'], 201);
        }
    	return response()->json(['error' => 'Some failure in sending friend request'], 401);
    }
    public function deleteFriend(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'friend_id' => 'required|numeric',
            'type' => 'required'
             //but in real, it is friends ID
        ]);
        if($validation->fails()) {
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        $result = UserRepositories::deleteRelationShip($request);
        if($result) {
            return response()->json(['success'], 200);
        }
        return response()->json(['error' => 'Relationship not found'], 404);
    }
    public function acceptFriend(Request $request)
    {
    	$validation = Validator::make($request->all(),[
    		'friend_id' => 'required|numeric' //but in real, it is friends ID
    	]);
    	if($validation->fails()) {
    		$errors = $validation->errors();
    		return response()->json($errors, 400);
    	}
        try{
            $friend = User::findorFail($request['friend_id']);
        } catch (\Exeption $e) {
            throw ModalNotFoundException;
        }
        $user = Auth::User();
    	$relationship = DB::table('user_friends')
                            ->where('user_id','=',$friend->id)
                            ->where('friend_id','=',$user->id)
                            ->first();

    	if($relationship) {
    		$result = DB::table('user_friends')
                            ->where('user_id','=',$friend->id)
                            ->where('friend_id','=',$user->id)
                            ->update(array('accepted'=> true));
            Mail::to($friend->email)->send(new RequestAccepted($user, $friend));
    		return response()->json(['success' => 'Request Accepted'], 201);
    	}
    	else {
    		return response()->json(['error' => 'No friend request was ever send'], 404);
    	}
    }
}
