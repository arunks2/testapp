<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoryController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $categories = Category::all();
       return response()->json($categories, 200);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $validation = Validator::make($request->all(),[
           'category_name' => 'required',
           'category_img' => 'sometimes|url' 
       ]);
       if($validation->fails()) {
           $errors = $validation->errors();
           return response()->json($errors, 400);
       }
       $category = $request->all();
       $category = Category::create($category);
       return response()->json($category, 201);

   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       try {
          $category = Category::findorFail($id);
       }catch (\Exception $e) {
          throw new ModalNotFoundException;
      }
      return response()->json($category->load('products'), 200);
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
       $validation = Validator::make($request->all(),[
           'category_name' => 'required',
           'category_img'  => 'sometimes|url'
       ]);
       if($validation->fails()) {
           $errors = $validation->errors();
           return response()->json($errors, 400);
       }
       try {
           $category = Category::findorFail($id);
       }catch (\Exception $e) {
           throw new ModalNotFoundException;
       }
       $category->update($request->all());
       return response()->json(true, 201);

   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
       try {
           $category = Category::findorFail($id);
       } catch (\Exception $e) {
         throw new ModalNotFoundException;
       }
       $result = $category->delete();
       return response()->json($result, 301);
   }
}
