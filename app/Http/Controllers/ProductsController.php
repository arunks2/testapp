<?php

namespace App\Http\Controllers;
use  App\Exceptions\ModalNotFoundException;
use Validator;
use Illuminate\Http\Request;
use App\Brand;
use App\Product;
use Auth;
use App\User;
use DB;
use App\Repositories\UserRepositories;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::getProducts($request);
        return response()->json($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
           'sku'   => 'required|unique:products,sku',
           'name'  => 'required',
           'img'   => 'sometimes|url',
           'attributes' => 'sometimes',
           'more-info' => 'sometimes',
           'brand_id' => 'sometimes|numeric',
           'category_id' => 'sometimes|numeric'
        ]);
        if($validation->fails()) {
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        $product = Product::createProduct($request);
        return response()->json($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addToWishlist(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'product_ids' => 'required'
        ]);

        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        $user = Auth::User();
        // dd($request['product_ids']);
        $result = $user->products()->sync($request['product_ids']);
        return response()->json($result, 201);
    }
    public function getLocalWishlist(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'product_ids' => 'required'
        ]);
        if($validation->fails()){
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        $products = Product::getProductByIds($request['product_ids']);
        return response()->json($products, 201);
    }
    public function getFriendWishlist($id)
    {
        try{
            $friend = User::findorFail($id);
        } catch (\Exception $e) {
            throw new ModalNotFoundException;
        }
        $user = Auth::User();
        $count = UserRepositories::checkIfRelationShipExist($user, $friend);
        if($count) {
            $products = Product::getFriendsProduct($friend);  
            return response()->json($products, 200);  
        }
        return response()->json(['error' => 'Relationship does not exist'], 404);
    }

    public function likeDislikeProduct(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'friend_id' => 'required|integer',
            'product_id' => 'required|integer',
            'type' => 'required|integer'
        ]);
        if($validation->fails()) {
            $errors = $validation->errors();
            return response()->json($errors, 400);
        }
        try {
            $friend = User::findorFail($request['friend_id']);
        } catch(\Exception $e) {
            throw new ModalNotFoundException;
        }
        $user = Auth::User();
        $count = UserRepositories::checkIfRelationShipExist($user, $friend);
        if($count) {
            try {
                $product = Product::findorFail($request['product_id']);
            }  catch(\Exception $e) {
                throw new ModalNotFoundException;
            }
            $result = Product::checkIfFriendHasProduct($friend,$product); 
            if($result) {
                $result = UserRepositories::checkIfUserHasLikedProduct($user, $product, $request['type']);
                return response()->json($result, 200);  
            }   
            else {
                return response()->json(['error' => 'Relationship does not exist'], 404);
            } 
        }
        return response()->json(['error' => 'Relationship does not exist'], 404);
    }

    public function sendWishList(Request $request)
    {
        $validation = Validator::make($request->all(),[
                   'product_ids' => 'required',
                   'customer_ids' => 'required'
               ]);

               if($validation->fails()){
                   $errors = $validation->errors();
                   return response()->json($errors, 400);
               }
               $user =  Auth::User();
               $products = Product::getProductByIds($request['product_ids']);
               $result = UserRepositories::sendWishList($request['customer_ids'],$products);
               return response()->json($result, 200);
    }
}
