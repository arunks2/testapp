<?php

namespace App\Listeners;

use App\Events\UserSignedUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contracts\SMScontract;
use Mail;
use App\Mail\Welcome;
class SendUserNotification
{

    public $sms;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SMScontract $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Handle the event.
     *
     * @param  UserSignedUp  $event
     * @return void
     */
    public function handle(UserSignedUp $event)
    {
        $user = $event->user;
        $message = "we welcome you on myapp, your otp code for verification is ". $user->sms_otp;
        if($user->mobile_number) {
            $this->sms->sendOTP($message, $user->mobile_number);
        }
        Mail::to($user->email)->send(new Welcome($user));
    }
}
