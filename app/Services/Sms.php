<?php

namespace App\Services;
use App\Contracts\SMScontract;
use Log;
class Sms implements SMScontract {

	protected $user;
	protected $hash;
	protected $sender;

	public function __construct($hash, $user, $sender) {
		$this->hash = $hash;
		$this->user = $user;
		$this->sender = $sender;
	}

	public function sendOTP($message, $number) {
		$username = $this->user;
		$hash = $this->hash;
		$sender = $this->sender;
		$message = rawurlencode($message);
		$data = array('username' => $username, 'hash' => $hash, 'numbers' => $number, "sender" => $sender, "message" => $message);
		
		// Send the POST request with cURL
		$ch = curl_init('http://api.textlocal.in/send/');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);
		Log::info('sent OTP via message: ' . $response);
	} 
}