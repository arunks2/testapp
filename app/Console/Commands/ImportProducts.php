<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use Goutte\Client;
use App\Product;
use App\Brand;
use App\Category;
class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {           
        $url = array();
        for($i=1;$i<29;$i++) {
             $url[] =  'https://www.appliancesdelivered.ie/search/small-appliances?sort=price_desc&page='.$i;
        }
        $url[] = 'https://www.appliancesdelivered.ie/dishwashers?page=1';
        $url[] = 'https://www.appliancesdelivered.ie/dishwashers?page=2';
        $client = new \Goutte\Client();
        for($j=0; $j<count($url);$j++) {
            $dom = $client->request('GET', $url[$j]);
            $results = [];
            foreach($dom->filter('div.search-results-product') as $element) {
                $results[] = ($element->textContent);
            }
            $productImage = array();
            $brandImage = array();
            $productName = array();
            $brands = array();
            $attributes = array();
            $previousPrice = array();
            $newPrice = array();
            $availabilty = array();
            $links = array();
            foreach ($dom->filter('img.img-responsive,img.article-brand,div.col-xs-12>h4>a, ul.result-list-item-desc-list, h5.price-previous, h3.section-title, a.item-info-more') as $element) {
                if ($element->getAttribute('class') =='img-responsive') { //product image
                    $productImage[] = $element->getAttribute('src');
                }
                if($element->getAttribute('class') == 'article-brand') { // brand img
                    $brandName = $element->getAttribute('alt');
                    $brandName_array = explode(' ',$brandName);
                    $brand = $brandName_array[0];
                    $brands[] = $brand;
                    $brandImage[$brand] = $element->getAttribute('src');
                }
                if($element->nodeName == 'a' && $element->getAttribute('class') != 'item-info-more' && $element->getAttribute('class') != 'section-title') { // node name
                    $productName[] = trim($element->textContent);
                    $links[] = trim($element->getAttribute('href'));
                }
                if ($element->nodeName == 'ul') {
                    $attributes[] = trim($element->textContent);
                }
                if ($element->getAttribute('class') == 'section-title') {
                    $newPrice[] = trim($element->textContent);
                }
                if ($element->getAttribute('class') == 'item-info-more') {
                    $availabilty[] = trim($element->textContent);
                }
            }
            // dd(($results[0]));
            // create brands 
            $brands = array();
            $skus = array();
            foreach ($productName as $name) {
                $name_array = explode(' ', $name);
                $length = count($name_array);
                $brands[] = $name_array[0];
                $skus[] = $name_array[$length-1];
            }
            
            // display products
            $category_name = 'Small Appliances';
            if($j>28) {
                $category_name = "Dishwasher";
            }
            $category = Category::where('category_name',$category_name)->first();
            if(!$category) {
                $category = Category::create(['category_name' => $category_name])->first();
            }
            $length = count($productName);
            for($i=0; $i < $length; $i++){
                $product = array();
                $product['name'] = $productName[$i];
                $product['img'] = $productImage[$i];
                $product['attributes'] = $attributes[$i];
                $product['more-info'] = $links[$i];
                $product['sku'] = $skus[$i];
                // return response()->json(strpos('Extended Warranty Available',$results[0]), 200);

                if(in_array('Warranty', explode(' ',$results[$i])))
                {
                    $product['is_warranty_available'] = 1;
                }
                if(in_array('Finance', explode(' ',$results[$i]))) {
                    $product['is_finance_available'] = 1;
                }
                $tempImage = array_key_exists($brands[$i], $brandImage) ? $brandImage[$brands[$i]] : Null;
                $brand = Brand::where('brand_name','=',$brands[$i])->first();
                if($brand) {
                    $brand->brand_img = $tempImage;
                    $brand->save();
                }
                else {
                    $brand = Brand::create(['brand_name' => $brands[$i],'brand_img'=>$tempImage]);
                }
                $product['brand_id'] = $brand->id;
                $product['category_id'] = $category->id;
                $newPrice[$i] = str_replace('€', '', $newPrice[$i]);
                $newPrice[$i] = str_replace(',', '', $newPrice[$i]);
                $product['price'] = (float)str_replace('€', '', $newPrice[$i]);
                // dd($product);
                $tempProduct = Product::where('sku', $product['sku'])->first();
                if($tempProduct) {
                    $tempProduct->update($product);
                }
                else {
                    $product = Product::create($product);
                }
            }
        }
    }
}
