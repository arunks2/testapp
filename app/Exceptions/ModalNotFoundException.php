<?php
namespace App\Exceptions;
class ModalNotFoundException extends \Exception
{ 
	protected $model;
	public function render($request)
    {
        return response()->json(['error'=>'Resource Not Found'], 404);
    }

}