<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';
    protected $fillable = ['brand_name','brand_img'];
    protected $dates = ['created_at','updated_at','deleted_at'];

    public function products()
    {
    	return $this->hasMany('App\Product','brand_id');
    }
    


}
