<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','mobile_number','is_mobile_verified','is_email_verified','sms_otp','email_otp','otp'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','sms_otp','email_otp','otp','is_email_verified','is_mobile_verified'
    ];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function friends()
    {
        return $this->belongsToMany('App\User', 'user_friends', 'user_id', 'friend_id')->withPivot('accepted')->withTimestamps();
    }
    // my invited friends
    public function myFriends()
    {
        return $this->belongsToMany('App\User', 'user_friends', 'user_id', 'friend_id')->withPivot('accepted')->withTimestamps(); // or to fetch accepted value
    }

    // friendship that I was invited to 
    function friendsWith()
    {
        return $this->belongsToMany('App\User','user_friends','friend_id','user_id')->withPivot('accepted')->withTimestamps();
    }

    public function getFriendsAttribute()
    {
        if (!array_key_exists('friends', $this->relations)) {
            $this->allFriends();
        }
        return $this->getRelation('friends');
    }

    protected function allFriends()
    {
        $friends = $this->mergeFriends();
        $this->setRelation('friends', $friends);
    }

    protected function mergeFriends()
    {
        return $this->myFriends->merge($this->friendsWith);
    }

    public function products()
    {
        return $this->belongsToMany('App\Product','user_products','user_id','product_id');
    }
    public function productsLiked()
    {
        return $this->belongsToMany('App\Product','user_product_like','user_id','product_id')->withPivot('likeability');
    } 
}
