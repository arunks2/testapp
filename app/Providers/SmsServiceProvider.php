<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use App\Services\Sms;
use App\Contracts\SMScontract;
class SmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('App\Contracts\SMScontract', function ($app) {
            return new Sms(config('services.textlocal.hash'), config('services.textlocal.user'),config('services.textlocal.sender'));
        });
    }
}
