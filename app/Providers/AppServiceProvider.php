<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); // schema patch
        Validator::extend('mobile_validation', 'App\Http\CustomValidator@mobileValidation');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
