<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ImportProductsService extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('App\Contracts\ImportProductsContract', function ($app) {
             
            return new ImportProductsContainer();
        });
    }
}
