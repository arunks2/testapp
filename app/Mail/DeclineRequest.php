<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeclineRequest extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $friend;
    private $type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $friend, $type)
    {
        $this->user = $user;
        $this->friend = $friend;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@myapp.com')
                    ->subject('MyApp: friends request declined!')
                     ->with([
                        'friend_name' => $this->friend->name,
                        'user_name' => $this->user->name,
                        'type' => $this->type
                    ])
                    ->view('emails.declinerequest');
    }
}
