<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
class SendFriendRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $friend;
    public $user;
    public function __construct(User $user, User $friend)
    {
       $this->user = $user;
       $this->friend = $friend;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@myapp.com')
                    ->subject('MyApp: '. $this->user->name .' has sent you a friend request')
                     ->with([
                        'friend_name' => $this->friend->name,
                        'user_name' => $this->user->name
                    ])
                    ->view('emails.sendrequest');
    }
}
