<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
class SendWishlist extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $products;
    public $user;
    public function __construct(User $user, $products)
    {
        $this->user = $user;
        $this->products = $products;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->from('noreply@myapp.com')
                ->subject('Myapp: Wishlist')
                ->with([
                    'products' => $this->products,
                    'user' => $this->user
                ])
                ->view('emails.wishlist');
    }
}
