<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Password Change Request</title>
</head>
<body>
	Hello {{$user->Name}}, <br>
	The otp for generating a new password is {{ $user->otp }}
	<br>
	Regards,<br>
	MyApp
</body>
</html>