<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to MyaPP</title>
</head>
<body>
		@if($type == 'denied') 
			Hello {{$friend_name}},<br>
			{{$user_name}} has declined your name. May be another time. </br>
		@elseif($type == 'unfriend')
			Hello {{$user_name}}, <br>
			{{$friend_name}} has unfriended you. <br>
		@endif
		<br>
		Regards, <br>
		Myapp Team	
</body>
</html>