<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to MyaPP</title>
</head>
<body>
		Hello {{$user->name}},
		<br>
		Thank you for registering with us. To activate your account use this {{$user->email_otp }}
		<br>
		Regards, <br>
		Myapp Team	
</body>
</html>