<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Target Integration</title>
</head>

<body style="padding:0; margin:0;">
  <div style="max-width: 600px; margin: 0 auto; background: #fefcf8; ">
    <div id="header" style=" background:#164968; width: 100%; text-align: center;">
      <img src="https://media.glassdoor.com/sql/1465362/target-integration-squarelogo-1479290890437.png" alt="Target Integration" style="padding: 12px;">
    </div>
    <div id="subheader" style="color:#3e3e34; background:#ffffff; padding: 10px 16px 10px; text-align: center;">
      YOUR WISHLIST
    </div>
    <div class="products-container" style='max-width:660px; padding: 0px 20px; margin-left: auto; margin-right: auto; margin-bottom:14px;'>
      <p style="padding: 2px 0px 12px; color: #333333;">
        Hello {{$user->name}},
        This is the wishlist that has been sent to you..
      </p>
      @foreach($products as $product)
                <div class="product" style="width:49%; float:left; margin-left: .5%; margin-right: .5%;">
                  <div class="product-inner">
                      <img src={{$product->img}} width="100%" height="auto" style="display:block; padding:4px; border: #dedede thin solid;">
                      <div class="name" style="margin-top:.5em; margin-bottom:.5em;"> {{$product->sku}}</div>
                  </div>
                </div>
      @endforeach
    </div>
    
    <div id="footer" style="color:#3f3f34; clear: both; text-align: center; line-height:1.6; font-size: 12px; padding:0 16px;">
      <div style="width:24px; margin: 18px auto; height: 2px; background: #666"></div>
      Target Integration
    </div>
  </div>
</body>
</html>