<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// $key = resolve('App\Services\Sms');
// dd($key);
Route::post('/signin', 'AuthenticationController@signIn');
Route::post('/signup', 'AuthenticationController@store');
Route::post('/logout', 'AuthenticationController@logout')->middleware('jwt.auth');
Route::post('/user', 'AuthenticationController@store')->middleware('jwt.auth');
Route::get('/user', 'AuthenticationController@index')->middleware('jwt.auth');
Route::get('/user/{id}', 'AuthenticationController@show')->middleware('jwt.auth');
Route::delete('/user/{id}', 'AuthenticationController@destroy')->middleware('jwt.auth');
Route::get('/test','AuthenticationController@authenticatedUser')->middleware('jwt.auth');

Route::post('/sms/verifyotp','AuthenticationController@verifySmsOTP')->middleware('jwt.auth');
Route::post('/email/verifyotp','AuthenticationController@verifyEmailOTP')->middleware('jwt.auth');


// friends
Route::get('/users','FriendsController@index')->middleware('jwt.auth');
Route::post('/friends','FriendsController@addFriend')->middleware('jwt.auth');
Route::post('/delete/friends','FriendsController@deleteFriend')->middleware('jwt.auth');
Route::patch('/friends','FriendsController@acceptFriend')->middleware('jwt.auth');

// forget password
Route::get('/password/otp','AuthenticationController@sendOTP'); // TODO::service working for now but check again, better write unit test for it
// Route::get('/email/otp','AuthenticationController@sendOTPViaEmail'); // TODO::may be, will do if any time is left.
Route::post('/password/otp','AuthenticationController@verifyOTP'); // TODO::service working for now but check again, better write unit test for it

Route::post('/changepassword','AuthenticationController@changePassword')->middleware('jwt.auth');

//crawler
Route::get('/getcrawling','AuthenticationController@crawlPage');


// products
Route::resource('/products','ProductsController');


Route::resource('/brands','BrandsController');
Route::get('/brands/{id}','BrandsController@show');
Route::resource('/categories','CategoryController');
// Add to cart

Route::post('/wishlist','ProductsController@addToWishlist')->middleware('jwt.auth');
Route::get('/local/wishlist','ProductsController@getLocalWishlist')->
	middleware('jwt.auth'); // just by local storage ids, depending upon request parameters
Route::get('/wishlist','ProductsController@getWishlist')->
	middleware('jwt.auth'); // saved wishlist of user
Route::get('/friend/{id}','ProductsController@getFriendWishlist')->
	middleware('jwt.auth'); // saved wishlist of friend
Route::post('/send/wishlist','ProductsController@sendWishlist')->
	middleware('jwt.auth');


// Like Dislike Product
Route::post('/like','ProductsController@likeDislikeProduct')->middleware('jwt.auth');
	
