# This app has been build using laravel

# Instruction for Running this app
# 1) Make sure your Homestead.yml file settings are following
	---
		ip: "192.168.10.10"
		memory: 2048
		cpus: 1
		provider: virtualbox

		authorize: ~/.ssh/id_rsa.pub

		keys:
			- ~/.ssh/id_rsa

		folders:
			- map: ~/Projects
			  to: /home/vagrant/code

		sites:
			- map: myapp.test
			  to: /home/vagrant/code/testapp/public
			  schedule: true
		databases:
			- homestead
# 2) In your hosts file map 192.168.10.10 to myapp.test
# 3) Git clone this repositiories run composer update and then composer dump-autoload
# 4) Copy .env.example to .env
# 5) In your .env file change database settings to your own, I have used default.
# 6) In your .env file Mail_Settings are currently using my account.
# 7) I have used Mailtrap as a fake smtp to send mail.You can use your own settings. If you want to use mailtrap.io, it is free.
# 8) Optional -- Register your self in mailtrap.io. Go to demo box now, under credentials you will find the settings that needs to replaced.
# 9) Again in your .env file, I am using textlocal.in to send sms. My limit is almost expiring so be considerate not to signup too many users.
# 10) Run php artisan migrate
# 11) Run php artisan schedule:import this will import all products in site
# 12) Run php artisan db:seed --class="UsersTableSeeder" to seed fake data for users
# 13) Api should run fine on myapp.test.
# 14) I have made many endpoints but was not able to consume all of them due to shortage of time.
# 15) Now go to myapp-ui readme.md