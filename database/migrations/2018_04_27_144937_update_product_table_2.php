<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table) {
            $table->dropColumn(['img','attributes']);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->text('img')->nullable()->after('name');
            $table->text('attributes')->nullable()->after('name');
            $table->integer('category_id')->unsigned()->after('name');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->boolean('is_finance_available')->default(false)->after('more-info');
            $table->boolean('is_warranty_available')->default(false)->after('more-info');
            $table->float('price', 8, 2)->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
            $table->dropColumn(['category_id','price','is_warranty_available','is_finance_available']);
        });
    }
}
