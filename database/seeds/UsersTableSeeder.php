<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        	foreach (range(1,10) as $index) {
    	        DB::table('users')->insert([
    	            'name' => $faker->name,
    	            'email' => $faker->unique()->email,
    	            'mobile_number' => $faker->unique()->phoneNumber,
    	            'is_email_verified' => true,
    	            'is_mobile_verified' => false,
    	            'password' => Hash::make(123456),
    	        ]);
		}
	}
}